
# Dark Crystal Key Backup Protocol Specification

![dark crystal icon](./assets/dark-crystal-icon_200x200.png)

This document has [moved to here](https://darkcrystal.pw/protocol-specification)
